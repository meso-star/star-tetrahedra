Note that this file format is *OBSOLETE* and therefore this library as well.
Instead, use the [Star-Mesh](https://gitlab.com/meso-star/star-mesh) format and
its associated library.

# Star-TetraHedra

Star-TetraHedra is a C library that loads tetrahedral meshes saved wrt the
Star-TetraHedra file format. 

## How to build

Star-TetraHedra is compatible GNU/Linux 64 bits and relies on the
[CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) packages to build. It also depends
on the [RSys](https://gitlab.com/vaplv/rsys/) library. It optionally depends on
the [AsciiDoc](https://asciidoc.org/) suite of tools; if available, the man
pages of the Star-TetraHedra file format will be generated.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as the aforementioned prerequisites. Finally generate the
project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directories of its dependencies. The
resulting project can be edited, built, tested and installed as any CMake
project. Refer to the [CMake](https://cmake.org/documentation) for further
informations on CMake.

## Release notes

### Version 0.1

Make file format and library obsolete. Refer to
[Star-Mesh](https://gitlab.com/meso-star/star-mesh) to replace them. This
version is therefore the last, and only adds features to facilitate the
transition.

- Add the `pagesize` member variable to the `sth_desc` structure
- Add the `sth2smsh` program that converts data from Star-Tetrahedra format to
  Star-Mesh format

## Copyright

Copyright (C) 2020-2023 [|Méso|Star>](https://www.meso-star.com)
(<contact@meso-star.com>)

## License

Star-TetraHedra is free software released under the GPL v3+ license: GNU GPL
version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.
