/* Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sth.h"
#include "test_sth_utils.h"

#include <rsys/double3.h>
#include <rsys/math.h>
#include <rsys/rsys.h>
#include <stdio.h>
#include <unistd.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
check_sth_desc
  (const struct sth_desc* desc,
   const uint64_t nverts,
   const uint64_t ntetra)
{
  size_t i;
  CHK(desc);
  CHK(nverts);
  CHK(ntetra);

  CHK(desc->nvertices == nverts);
  CHK(desc->ntetrahedra == ntetra);

  FOR_EACH(i, 0, nverts) {
    const double* pos0 = desc->positions + i*3;
    const double* pos1 = sth_desc_get_vertex_position(desc, i);
    CHK(d3_eq(pos0, pos1));
    CHK(pos0[0] == (double)i + 0.0);
    CHK(pos0[1] == (double)i + 0.1);
    CHK(pos0[2] == (double)i + 0.2);
  }

  FOR_EACH(i, 0, ntetra) {
    const uint64_t* ids0 = desc->indices + i*4;
    const uint64_t* ids1 = sth_desc_get_tetrahedron_indices(desc, i);
    CHK(ids0[0] == ids1[0]);
    CHK(ids0[1] == ids1[1]);
    CHK(ids0[2] == ids1[2]);
    CHK(ids0[3] == ids1[3]);
    CHK(ids0[0] == (i*4+0)%nverts);
    CHK(ids0[1] == (i*4+1)%nverts);
    CHK(ids0[2] == (i*4+2)%nverts);
    CHK(ids0[3] == (i*4+3)%nverts);
  }
}

static void
test_load(struct sth* sth)
{
  struct sth_desc desc = STH_DESC_NULL;
  FILE* fp = NULL;
  const char* filename = "test_file.sth";
  const uint64_t pagesize = 16384;
  const uint64_t nverts = 287;
  const uint64_t ntetra = 192;
  size_t i;
  char byte = 0;
  ASSERT(sth);

  fp = fopen(filename, "w+");
  CHK(fp);

  /* Write file header */
  CHK(fwrite(&pagesize, sizeof(pagesize), 1, fp) == 1);
  CHK(fwrite(&nverts, sizeof(nverts), 1, fp) == 1);
  CHK(fwrite(&ntetra, sizeof(ntetra), 1, fp) == 1);

  /* Padding */
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize), SEEK_SET) == 0);

  /* Write vertex positions */
  FOR_EACH(i, 0, nverts) {
    double pos[3];
    pos[0] = (double)i + 0.0;
    pos[1] = (double)i + 0.1;
    pos[2] = (double)i + 0.2;
    CHK(fwrite(pos, sizeof(*pos), 3, fp) == 3);
  }

  /* Padding */
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize), SEEK_SET) == 0);

  /* Write tetrahedra */
  FOR_EACH(i, 0, ntetra) {
    uint64_t ids[4];
    ids[0] = (i*4 + 0) % nverts;
    ids[1] = (i*4 + 1) % nverts;
    ids[2] = (i*4 + 2) % nverts;
    ids[3] = (i*4 + 3) % nverts;
    CHK(fwrite(ids, sizeof(*ids), 4, fp) == 4);
  }

  /* Padding. Write one char to position the EOF indicator */
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize)-1, SEEK_SET) == 0);
  CHK(fwrite(&byte, sizeof(byte), 1, fp) == 1);

  rewind(fp);
  CHK(sth_load_stream(NULL, fp, filename) == RES_BAD_ARG);
  CHK(sth_load_stream(sth, NULL, filename) == RES_BAD_ARG);
  CHK(sth_load_stream(sth, fp, NULL) == RES_OK);
  CHK(sth_get_desc(NULL, &desc) == RES_BAD_ARG);
  CHK(sth_get_desc(sth, NULL) == RES_BAD_ARG);
  CHK(sth_get_desc(sth, &desc) == RES_OK);
  check_sth_desc(&desc, nverts, ntetra);

  rewind(fp);
  CHK(sth_load_stream(sth, fp, filename) == RES_OK);
  check_sth_desc(&desc, nverts, ntetra);

  CHK(sth_load(NULL, filename) == RES_BAD_ARG);
  CHK(sth_load(sth, NULL) == RES_BAD_ARG);
  CHK(sth_load(sth, "nop") == RES_IO_ERR);
  CHK(sth_load(sth, filename) == RES_OK);
  check_sth_desc(&desc, nverts, ntetra);

  fclose(fp);
}

static void
test_load_fail(struct sth* sth)
{
  const char byte = 0;
  FILE* fp = NULL;
  uint64_t pagesize;
  uint64_t nverts;
  uint64_t ntetra;

  /* Wrong pagesize */
  fp = tmpfile();
  CHK(fp);
  pagesize = 1023;
  nverts = 10;
  ntetra = 10;
  CHK(fwrite(&pagesize, sizeof(pagesize), 1, fp) == 1);
  CHK(fwrite(&nverts, sizeof(nverts), 1, fp) == 1);
  CHK(fwrite(&ntetra, sizeof(ntetra), 1, fp) == 1);
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize), SEEK_SET) == 0);
  CHK(fseek(fp, (long)(sizeof(double[3])*nverts), SEEK_CUR) == 0);
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize), SEEK_SET) == 0);
  CHK(fseek(fp, (long)(sizeof(uint64_t[4])*ntetra), SEEK_CUR) == 0);
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize)-1, SEEK_SET) == 0);
  CHK(fwrite(&byte, sizeof(byte), 1, fp) == 1);
  rewind(fp);
  CHK(sth_load_stream(sth, fp, NULL) == RES_BAD_ARG);
  fclose(fp);

  /* Wrong size */
  fp = tmpfile();
  CHK(fp);
  pagesize = (uint64_t)sysconf(_SC_PAGESIZE);
  nverts = 10;
  ntetra = 10;
  CHK(fwrite(&pagesize, sizeof(pagesize), 1, fp) == 1);
  CHK(fwrite(&nverts, sizeof(nverts), 1, fp) == 1);
  CHK(fwrite(&ntetra, sizeof(ntetra), 1, fp) == 1);
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize), SEEK_SET) == 0);
  CHK(fseek(fp, (long)(sizeof(double[3])*nverts), SEEK_CUR) == 0);
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize), SEEK_SET) == 0);
  CHK(fseek(fp, (long)(sizeof(uint64_t[4])*ntetra), SEEK_CUR) == 0);
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize)-2, SEEK_SET) == 0);
  CHK(fwrite(&byte, sizeof(byte), 1, fp) == 1);
  rewind(fp);
  CHK(sth_load_stream(sth, fp, NULL) == RES_IO_ERR);
  fclose(fp);
}

static void
test_load_files(struct sth* sth, int argc, char** argv)
{
  int i;
  CHK(sth);
  FOR_EACH(i, 1, argc) {
    struct sth_desc desc = STH_DESC_NULL;
    size_t ivert;
    size_t itetra;

    printf("Load %s\n", argv[i]);
    CHK(sth_load(sth, argv[i]) == RES_OK);
    CHK(sth_get_desc(sth, &desc) == RES_OK);

    FOR_EACH(ivert, 0, desc.nvertices) {
      const double* pos = sth_desc_get_vertex_position(&desc, ivert);
      CHK(pos[0] == pos[0]); /* !NaN */
      CHK(pos[1] == pos[1]); /* !NaN */
      CHK(pos[2] == pos[2]); /* !NaN */
      CHK(!IS_INF(pos[0]));
      CHK(!IS_INF(pos[1]));
      CHK(!IS_INF(pos[2]));
    }
    FOR_EACH(itetra, 0, desc.ntetrahedra) {
      const uint64_t* ids = sth_desc_get_tetrahedron_indices(&desc, itetra);
      /* Check non degenerated tetrahedron */
      CHK(ids[0] != ids[1]);
      CHK(ids[0] != ids[2]);
      CHK(ids[0] != ids[3]);
      CHK(ids[1] != ids[2]);
      CHK(ids[1] != ids[3]);
      CHK(ids[2] != ids[3]);
    }
  }
}

/*******************************************************************************
 * Main function
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct sth* sth;
  (void)argc, (void)argv;

  CHK(sth_create(NULL, &mem_default_allocator, 1, &sth) == RES_OK);

  test_load(sth);
  test_load_fail(sth);
  test_load_files(sth, argc, argv);

  CHK(sth_ref_put(sth) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}
