/* Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200809L /* mmap support */
#define _DEFAULT_SOURCE 1 /* MAP_POPULATE support */
#define _BSD_SOURCE 1 /* MAP_POPULATE for glibc < 2.19 */

#include "sth.h"
#include "sth_c.h"
#include "sth_log.h"

#include <rsys/mem_allocator.h>

#include <errno.h>
#include <unistd.h>
#include <sys/mman.h> /* mmap */

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
reset_sth(struct sth* sth)
{
  ASSERT(sth);
  sth->nvertices = 0;
  sth->ntetrahedra = 0;
  sth->pagesize = 0;
  if(sth->pos) munmap(sth->pos, sth->map_len_pos);
  if(sth->ids) munmap(sth->ids, sth->map_len_ids);
  sth->pos = NULL;
  sth->ids = NULL;
  sth->map_len_pos = 0;
  sth->map_len_ids = 0;
}

static res_T
map_data
  (struct sth* sth,
   const char* stream_name,
   const int fd, /* File descriptor */
   const size_t filesz, /* Overall filesize */
   const char* data_name,
   const off_t offset, /* Offset of the data into file */
   const size_t map_len,
   void** out_map) /* Lenght of the data to map */
{
  void* map = NULL;
  res_T res = RES_OK;
  ASSERT(sth && stream_name && filesz && data_name && map_len && out_map);
  ASSERT(IS_ALIGNED((size_t)offset, (size_t)sth->pagesize));

  if((size_t)offset + map_len > filesz) {
    log_err(sth, "%s: the %s to load exceed the file size.\n",
      stream_name, data_name);
    res = RES_IO_ERR;
    goto error;
  }

  map = mmap(NULL, map_len, PROT_READ, MAP_PRIVATE|MAP_POPULATE, fd, offset);
  if(map == MAP_FAILED) {
    log_err(sth, "%s: could not map the %s -- %s.\n",
      stream_name, data_name, strerror(errno));
    res = RES_IO_ERR;
    goto error;
  }

exit:
  *out_map = map;
  return res;
error:
  if(map == MAP_FAILED) map = NULL;
  goto exit;
}

static res_T
load_stream(struct sth* sth, FILE* stream, const char* stream_name)
{
  off_t pos_offset;
  off_t ids_offset;
  size_t filesz;
  res_T res = RES_OK;
  ASSERT(sth && stream && stream_name);

  reset_sth(sth);

  /* Read file header */
  #define READ(Var, N, Name) {                                                 \
    if(fread((Var), sizeof(*(Var)), (N), stream) != (N)) {                     \
      log_err(sth, "%s: could not read the %s.\n", stream_name, (Name));       \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&sth->pagesize, 1, "page size");
  READ(&sth->nvertices, 1, "number of vertices");
  READ(&sth->ntetrahedra, 1, "number of tetrahedra");
  #undef READ

  if(!IS_ALIGNED(sth->pagesize, sth->pagesize_os)) {
    log_err(sth,
      "%s: invalid page size %li. The page size attribute must be aligned on "
      "the page size of the operating system (%lu).\n",
      stream_name, sth->pagesize, (unsigned long)sth->pagesize_os);
    res = RES_BAD_ARG;
    goto error;
  }

  /* Compute the length in bytes of the data to map */
  sth->map_len_pos = sth->nvertices * sizeof(double[3]);
  sth->map_len_ids = sth->ntetrahedra * sizeof(uint64_t[4]);
  sth->map_len_pos = ALIGN_SIZE(sth->map_len_pos, (size_t)sth->pagesize);
  sth->map_len_ids = ALIGN_SIZE(sth->map_len_ids, (size_t)sth->pagesize);

  /* Find the offsets of the positions/indices data into the stream */
  pos_offset = (off_t)ALIGN_SIZE((uint64_t)ftell(stream), sth->pagesize);
  ids_offset = (off_t)((size_t)pos_offset + sth->map_len_pos);

  /* Retrieve the overall filesize */
  fseek(stream, 0, SEEK_END);
  filesz = (size_t)ftell(stream);

  /* Map the positions */
  res = map_data(sth, stream_name, fileno(stream), filesz, "positions",
    pos_offset, sth->map_len_pos, (void**)&sth->pos);
  if(res != RES_OK) goto error;

  /* Map the indices */
  res = map_data(sth, stream_name, fileno(stream), filesz, "indices",
    ids_offset, sth->map_len_ids, (void**)&sth->ids);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  reset_sth(sth);
  goto exit;
}

static void
release_sth(ref_T* ref)
{
  struct sth* sth;
  ASSERT(ref);
  sth = CONTAINER_OF(ref, struct sth, ref);
  reset_sth(sth);
  if(sth->logger == &sth->logger__) logger_release(&sth->logger__);
  MEM_RM(sth->allocator, sth);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
sth_create
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* mem_allocator, /* NULL <=> use default allocator */
   const int verbose,
   struct sth** out_sth)
{
  struct sth* sth = NULL;
  struct mem_allocator* allocator = NULL;
  res_T res = RES_OK;

  if(!out_sth) {
    res = RES_BAD_ARG;
    goto error;
  }

  allocator = mem_allocator ? mem_allocator : &mem_default_allocator;
  sth = MEM_CALLOC(allocator, 1, sizeof(*sth));
  if(!sth) {
    if(verbose) {
      #define ERR_STR "Could not allocate the Star-TetraHedra device.\n"
      if(logger) {
        logger_print(logger, LOG_ERROR, ERR_STR);
      } else {
        fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
      }
      #undef ERR_STR
    }
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&sth->ref);
  sth->allocator = allocator;
  sth->verbose = verbose;
  sth->pagesize_os = (size_t)sysconf(_SC_PAGESIZE);
  if(logger) {
    sth->logger = logger;
  } else {
    setup_log_default(sth);
  }

exit:
  if(out_sth) *out_sth = sth;
  return res;
error:
  if(sth) {
    STH(ref_put(sth));
    sth = NULL;
  }
  goto exit;
}

res_T
sth_ref_get(struct sth* sth)
{
  if(!sth) return RES_BAD_ARG;
  ref_get(&sth->ref);
  return RES_OK;
}

res_T
sth_ref_put(struct sth* sth)
{
  if(!sth) return RES_BAD_ARG;
  ref_put(&sth->ref, release_sth);
  return RES_OK;
}


res_T
sth_load(struct sth* sth, const char* path)
{
  FILE* file = NULL;
  res_T res = RES_OK;

  if(!sth || !path) {
    res = RES_BAD_ARG;
    goto error;
  }

  file = fopen(path, "r");
  if(!file) {
    log_err(sth, "%s: error opening file `%s'.\n", FUNC_NAME, path);
    res = RES_IO_ERR;
    goto error;
  }

  res = load_stream(sth, file, path);
  if(res != RES_OK) goto error;

exit:
  if(file) fclose(file);
  return res;
error:
  goto exit;
}

res_T
sth_load_stream
  (struct sth* sth,
   FILE* stream,
   const char* stream_name)
{
  if(!sth || !stream) return RES_BAD_ARG;
  return load_stream(sth, stream, stream_name ? stream_name : "<stream>");
}

res_T
sth_get_desc(const struct sth* sth, struct sth_desc* desc)
{
  if(!sth || !desc) return RES_BAD_ARG;
  desc->positions = sth->pos;
  desc->indices = sth->ids;
  desc->nvertices = sth->nvertices;
  desc->ntetrahedra = sth->ntetrahedra;
  desc->pagesize = sth->pagesize;
  return RES_OK;
}
