/* Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sth.h"

#include <rsys/cstr.h>
#include <rsys/rsys.h>
#include <getopt.h>

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static void
print_synopsis(const char* cmd)
{
  ASSERT(cmd);
  fprintf(stderr, "Usage: %s < input > output\n", cmd);
}

static void
print_help(const char* cmd)
{
  ASSERT(cmd);
  print_synopsis(cmd);
  fprintf(stderr,
"Converts data read from standard input from sth(5) format to \n"
"smsh(5) format and writes the result to standard output\n\n");
  fprintf(stderr,
"  -h             display this help and exit\n\n");
  fprintf(stderr,
"This is free software released under the GNU GPL license, version 3 or\n"
"later. You are free to change or redistribute it under certain\n"
"conditions <http://gnu.org.licenses/gpl.html>\n");
}

static res_T
parse_args(int argc, char** argv, int* quit)
{
  int opt;
  res_T res = RES_OK;
  ASSERT(argc && argv && quit);

  *quit = 0;
  while((opt = getopt(argc, argv, "h")) != -1) {
    switch(opt) {
      case 'h':
        print_help(argv[0]);
        *quit = 1;
        goto exit;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      print_synopsis(argv[0]);
      goto error;
    }
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Main program
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct sth_desc desc = STH_DESC_NULL;
  struct sth* sth = NULL;
  const uint32_t dnode = 3;
  const uint32_t dcell = 4;
  size_t i;
  int quit = 0;
  int err = 0;
  res_T res = RES_OK;
  char* padding = NULL;
  STATIC_ASSERT(sizeof(size_t) == 8, Unexpected_sizeof_size_t);

  if((res = parse_args(argc, argv, &quit)) != RES_OK) goto error;
  if(quit) goto exit;

  if((res = sth_create(NULL, NULL, 1, &sth)) != RES_OK) goto error;
  if((res = sth_load_stream(sth, stdin, NULL)) != RES_OK) goto error;
  if((res = sth_get_desc(sth, &desc)) != RES_OK) goto error;

  padding = calloc(desc.pagesize, 1);
  if(!padding) { res = RES_MEM_ERR; goto error; }

  #define WRITE(Var, N) {                                                      \
    if(fwrite((Var), sizeof(*(Var)), (N), stdout) != (N)) {                    \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  WRITE(&desc.pagesize, 1);
  WRITE(&desc.nvertices, 1);
  WRITE(&desc.ntetrahedra, 1);
  WRITE(&dnode, 1);
  WRITE(&dcell, 1);

  /* Padding */
  WRITE(padding, desc.pagesize-32/*sizeof(header)*/);

  WRITE(desc.positions, desc.nvertices*dnode);

  /* Padding */
  i = (desc.nvertices*dnode*sizeof(*desc.positions)) % desc.pagesize;
  WRITE(padding, desc.pagesize - i);

  WRITE(desc.indices, desc.ntetrahedra*dcell);

  /* Padding */
  i = (desc.ntetrahedra*dcell*sizeof(*desc.indices)) % desc.pagesize;
  WRITE(padding, desc.pagesize - i);

  fflush(stdout);

exit:
  if(sth) STH(ref_put(sth));
  if(padding) free(padding);
  return err;
error:
  fprintf(stderr, "error: %s\n", res_to_cstr(res));
  err = 1;
  goto exit;
}
