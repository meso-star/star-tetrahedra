/* Copyright (C) 2020-2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef STH_H
#define STH_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(STH_SHARED_BUILD) /* Build shared library */
  #define STH_API extern EXPORT_SYM
#elif defined(STH_STATIC) /* Use/build static library */
  #define STH_API extern LOCAL_SYM
#else /* Use shared library */
  #define STH_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the sth function `Func'
 * returns an error. One should use this macro on sth function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define STH(Func) ASSERT(sth_ ## Func == RES_OK)
#else
  #define STH(Func) sth_ ## Func
#endif

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;

/* Forward declaration of opaque data types */
struct sth;

struct sth_desc {
  const double* positions; /* List of double[3] */
  const uint64_t* indices; /* List of uint64_t[4] */
  size_t nvertices;
  size_t ntetrahedra;
  size_t pagesize;
};
static const struct sth_desc STH_DESC_NULL;

BEGIN_DECLS

/*******************************************************************************
 * STH API
 ******************************************************************************/
STH_API res_T
sth_create
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* allocator, /* NULL <=> use default allocator */
   const int verbose, /* Verbosity level */
   struct sth** sth);

STH_API res_T
sth_ref_get
  (struct sth* sth);

STH_API res_T
sth_ref_put
  (struct sth* sth);

STH_API res_T
sth_load
  (struct sth* sth,
   const char* path);

STH_API res_T
sth_load_stream
  (struct sth* sth,
   FILE* stream,
   const char* stream_name); /* Can be NULL */

STH_API res_T
sth_get_desc
  (const struct sth* sth,
   struct sth_desc* desc);

static INLINE const double*
sth_desc_get_vertex_position
  (const struct sth_desc* desc,
   const size_t ivertex)
{
  ASSERT(desc && ivertex < desc->nvertices);
  return desc->positions + ivertex*3/*#coords per vertex*/;
}

static INLINE const uint64_t*
sth_desc_get_tetrahedron_indices
  (const struct sth_desc* desc,
   const size_t itetra)
{
  const uint64_t* indices;
  ASSERT(desc && itetra < desc->ntetrahedra);
  indices = desc->indices + itetra*4/*#vertices per tetrahedron*/;
  ASSERT(indices[0] < desc->nvertices);
  ASSERT(indices[1] < desc->nvertices);
  ASSERT(indices[2] < desc->nvertices);
  ASSERT(indices[3] < desc->nvertices);
  return indices;
}

END_DECLS

#endif /* STH_H */
